<!DOCTYPE html>
<html lang="en">

<head>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width,initial-scale=1">
	<link rel="icon" href="/logo.png">
	<link rel="alternate" type="application/rss+xml" href="/rss.xml" title="My words on the Internet.">
	
<title>Tailwind CSS: Trading Looks for Looks</title>
<meta name="description" content="I didn&#x27;t like the practice of &quot;polluting&quot; my HTML with classes. That&#x27;s why I was ignoring Tailwind. But judging things without trying them isn&#x27;t how I want to be. That&#x27;s why I was willing to give it a try.">
<style>
	article>* {
		margin: 1.5rem 0;
		font-weight: 300;
	}

	ul {
		list-style-type: square;
	}

	pre {
		padding: 0.75rem;
		overflow-x: auto;
		font-weight: 400;
		line-height: 1.5;
	}

	code {
		font-family: ui-monospace, 'Cascadia Code', 'Source Code Pro', Menlo, Consolas, 'DejaVu Sans Mono', monospace;
	}
</style>

	<style>
		body {
			max-width: 100ch;
			margin: auto;
			padding: 2rem 1rem 4rem;
			font: 1.5rem/2 Avenir, Montserrat, Corbel, 'URW Gothic', source-sans-pro, sans-serif;
		}

		header {
			margin-bottom: 4rem;
			display: flex;
			justify-content: flex-end;
		}

		nav {
			display: flex;
			align-items: flex-end;
			gap: 1rem;
			font-family: Charter, 'Bitstream Charter', 'Sitka Text', Cambria, serif;
		}

		main {
			max-width: 60ch;
			margin: auto;
		}

		h1,
		h2,
		h3 {
			font-family: Charter, 'Bitstream Charter', 'Sitka Text', Cambria, serif;
			font-weight: bold;
		}

		h2,
		h3 {
			margin-top: 4.5rem;
		}

		h1 {
			font-size: 2.1rem;
		}

		h2 {
			font-size: 1.8rem;
		}

		h3 {
			font-size: 1.5rem;
		}

		a,
		a:visited {
			color: #000;
			text-underline-position: under;
		}

		.list {
			margin: 0;
			padding: 0;
		}

		.list-item {
			margin: 2rem 0;
			font-family: Charter, 'Bitstream Charter', 'Sitka Text', Cambria, serif;
			font-size: 2.1rem;
			font-weight: bold;
			list-style: none;
		}

		@media screen and (max-width: 500px) {
			body {
				font-size: 1.2rem;
				line-height: 1.8;
			}

			h1 {
				font-size: 1.8rem;
			}

			h2 {
				font-size: 1.5rem;
			}

			h3 {
				font-size: 1.2rem;
			}

			a {
				text-underline-offset: -0.2rem;
			}

			.list-item {
				margin: 1.5rem 0;
				font-size: 1.8rem;
			}
		}

		@media (hover:hover) {
			a:hover {
				opacity: 0.6;
			}
		}
	</style>
</head>

<body>
	<header>
		<nav>
			<a href="/" aria-label="Home">Home</a>
			<a href="https://codeberg.org/serghei" aria-label="My Codeberg profiles">Codeberg</a>
			<a href="https://sergheis.com/rss.xml" aria-label="My blog's rss.xml">RSS</a>
		</nav>
	</header>
	<main>
		
<article>
	<h1>Tailwind CSS: Trading Looks for Looks</h1>
	<time datetime=2023-04-24>April 24, 2023</time>
	<p>Exploring the ecosystem of web development, one will stumble upon
<a rel="nofollow noreferrer" href="https://tailwindcss.com/">Tailwind CSS</a>. While redesigning my website, I
decided to give Tailwind a try.</p>
<h2 id="what-s-tailwind">What's Tailwind?</h2>
<p>Tailwind is a CSS framework for building websites. With it, you don’t write
classic CSS. Instead, Tailwind provides thousands of predefined classes to use
inside your HTML.</p>
<p>You can compare it to Atomic CSS. Quoting
<a rel="nofollow noreferrer" href="https://css-tricks.com/lets-define-exactly-atomic-css/">CSS-Tricks</a>:</p>
<blockquote>
<p>Atomic CSS is the approach to CSS architecture that favors small,
single-purpose classes with names based on visual function.</p>
</blockquote>
<p>With an Atomic CSS approach, you might create the following class for a flex
layout:</p>
<pre data-lang="css" style="background-color:#151515;color:#e8e8d3;" class="language-css "><code class="language-css" data-lang="css"><span style="color:#ffb964;">.flex </span><span>{
</span><span>    display: flex;
</span><span>}
</span></code></pre>
<p>That's pretty much the same thing with Tailwind. One difference is that you
don't have to write these classes yourself. These classes come with Tailwind.</p>
<p>But Tailwind is more than Atomic CSS with predefined classes. So let's talk
about the good, the bad, and the ugly.</p>
<h2 id="the-good">The Good</h2>
<p>Coming up with class names gets more difficult as the project grows. For big
projects, this becomes very noticeable. With Tailwind, you won’t have this
problem. Yes, there will be cases where you’ll still need class names. And with
Tailwind, the ones you’ll come up with will likely still be available.</p>
<p>Not only does Tailwind save you time coming up with names, but it also speeds up
your work. Those predefined classes have shorter names than their styles written
in CSS. At least most of the time.</p>
<p>Let's compare it by writing the same HTML without Tailwind and then with it.</p>
<pre data-lang="html" style="background-color:#151515;color:#e8e8d3;" class="language-html "><code class="language-html" data-lang="html"><span style="color:#888888;">&lt;!-- Classic CSS --&gt;
</span><span>&lt;</span><span style="color:#ffb964;">ul class</span><span>=</span><span style="color:#556633;">&quot;</span><span style="color:#99ad6a;">list</span><span style="color:#556633;">&quot;</span><span>&gt;
</span><span>    &lt;</span><span style="color:#ffb964;">li</span><span>&gt;Item 1&lt;/</span><span style="color:#ffb964;">li</span><span>&gt;
</span><span>    &lt;</span><span style="color:#ffb964;">li</span><span>&gt;Item 2&lt;/</span><span style="color:#ffb964;">li</span><span>&gt;
</span><span>    &lt;</span><span style="color:#ffb964;">li</span><span>&gt;Item 3&lt;/</span><span style="color:#ffb964;">li</span><span>&gt;
</span><span>&lt;/</span><span style="color:#ffb964;">ul</span><span>&gt;
</span><span>
</span><span>&lt;</span><span style="color:#ffb964;">style</span><span>&gt;
</span><span>    </span><span style="color:#ffb964;">.list </span><span>{
</span><span>        margin: </span><span style="color:#cf6a4c;">0</span><span>;
</span><span>        padding: </span><span style="color:#cf6a4c;">0</span><span>;
</span><span>        list-style: none;
</span><span>  }
</span><span>&lt;/</span><span style="color:#ffb964;">style</span><span>&gt;
</span></code></pre>
<pre data-lang="html" style="background-color:#151515;color:#e8e8d3;" class="language-html "><code class="language-html" data-lang="html"><span style="color:#888888;">&lt;!-- Tailwind --&gt;
</span><span>&lt;</span><span style="color:#ffb964;">ul class</span><span>=</span><span style="color:#556633;">&quot;</span><span style="color:#99ad6a;">m-0 list-none p-0</span><span style="color:#556633;">&quot;</span><span>&gt;
</span><span>    &lt;</span><span style="color:#ffb964;">li</span><span>&gt;Item 1&lt;/</span><span style="color:#ffb964;">li</span><span>&gt;
</span><span>    &lt;</span><span style="color:#ffb964;">li</span><span>&gt;Item 2&lt;/</span><span style="color:#ffb964;">li</span><span>&gt;
</span><span>    &lt;</span><span style="color:#ffb964;">li</span><span>&gt;Item 3&lt;/</span><span style="color:#ffb964;">li</span><span>&gt;
</span><span>&lt;/</span><span style="color:#ffb964;">ul</span><span>&gt;
</span></code></pre>
<p>In both cases, we remove the margin and the padding from the list while changing
the list style to none. It's also common to split up HTML and CSS into separate
files, having to maintain more files. Tailwind spares you that extra work.</p>
<p>Splitting HTML and CSS can lead to bigger-than-needed CSS files. It’s common to
delete an element and forget the belonging CSS class. Not only bloating your CSS
file/s but also creating zombie classes.</p>
<p>The developer experience is also way better with Tailwind. Aside from the
positives mentioned before, Tailwind also handles specific vendor prefixes. Thus
further reducing your time writing CSS. And that's still not all. They also
offer a VSCode extension that adds IntelliSense for Tailwind classes.</p>
<p>At last, you don't have to stick to Tailwind's predefined values. You can
customize them as you'd like it. You can replace the given styles or add new
ones as you wish.</p>
<h2 id="the-bad-and-the-ugly">The Bad and the Ugly</h2>
<p>In most cases, Tailwind won’t work immediately after installing it. You need to
configure it before it works, thus taking some time to get started.</p>
<p>Another problem with Tailwind is repeating code. If you write proper classes,
this shouldn’t happen with classic CSS. In modern frameworks, you can create
components to combat the problem. With the directive <code>@apply</code>, you can apply
Tailwind classes in a CSS file. This way, you can make classes that contain
many Tailwind classes.</p>
<p>And as you might know, the worst thing about Tailwind is the HTML. It gets
polluted with classes and becomes ugly and unreadable very fast.</p>
<h2 id="final-words">Final Words</h2>
<p>As you might have noticed, I shared more positives than negatives. That doesn't
mean everything I mentioned is comparable one-to-one. As with everything, in the
end, it depends on your taste and preferences. But no matter if the good
outweighs the bad or the other way around. Tailwind is a technology you should
at least try to use before judging it.</p>
<p>Tailwind is a great tool that I’ll keep using in the future. As of right now, I
am still not a fan of how it makes my HTML look. But spending less time styling
and more time focusing on what I like the most is a big plus. So I’ll accept the
negatives, as the positives are too good to ignore.</p>

</article>

	</main>
</body>

</html>
