<!DOCTYPE html>
<html lang="en">

<head>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width,initial-scale=1">
	<link rel="icon" href="/logo.png">
	<link rel="alternate" type="application/rss+xml" href="/rss.xml" title="My words on the Internet.">
	
<title>Let It Crash</title>
<meta name="description" content="Using asserts and letting programs crash.">
<style>
	article>* {
		margin: 1.5rem 0;
		font-weight: 300;
	}

	ul {
		list-style-type: square;
	}

	pre {
		padding: 0.75rem;
		overflow-x: auto;
		font-weight: 400;
		line-height: 1.5;
	}

	code {
		font-family: ui-monospace, 'Cascadia Code', 'Source Code Pro', Menlo, Consolas, 'DejaVu Sans Mono', monospace;
	}
</style>

	<style>
		body {
			max-width: 100ch;
			margin: auto;
			padding: 2rem 1rem 4rem;
			font: 1.5rem/2 Avenir, Montserrat, Corbel, 'URW Gothic', source-sans-pro, sans-serif;
		}

		header {
			margin-bottom: 4rem;
			display: flex;
			justify-content: flex-end;
		}

		nav {
			display: flex;
			align-items: flex-end;
			gap: 1rem;
			font-family: Charter, 'Bitstream Charter', 'Sitka Text', Cambria, serif;
		}

		main {
			max-width: 60ch;
			margin: auto;
		}

		h1,
		h2,
		h3 {
			font-family: Charter, 'Bitstream Charter', 'Sitka Text', Cambria, serif;
			font-weight: bold;
		}

		h2,
		h3 {
			margin-top: 4.5rem;
		}

		h1 {
			font-size: 2.1rem;
		}

		h2 {
			font-size: 1.8rem;
		}

		h3 {
			font-size: 1.5rem;
		}

		a,
		a:visited {
			color: #000;
			text-underline-position: under;
		}

		.list {
			margin: 0;
			padding: 0;
		}

		.list-item {
			margin: 2rem 0;
			font-family: Charter, 'Bitstream Charter', 'Sitka Text', Cambria, serif;
			font-size: 2.1rem;
			font-weight: bold;
			list-style: none;
		}

		@media screen and (max-width: 500px) {
			body {
				font-size: 1.2rem;
				line-height: 1.8;
			}

			h1 {
				font-size: 1.8rem;
			}

			h2 {
				font-size: 1.5rem;
			}

			h3 {
				font-size: 1.2rem;
			}

			a {
				text-underline-offset: -0.2rem;
			}

			.list-item {
				margin: 1.5rem 0;
				font-size: 1.8rem;
			}
		}

		@media (hover:hover) {
			a:hover {
				opacity: 0.6;
			}
		}
	</style>
</head>

<body>
	<header>
		<nav>
			<a href="/" aria-label="Home">Home</a>
			<a href="https://codeberg.org/serghei" aria-label="My Codeberg profiles">Codeberg</a>
			<a href="https://sergheis.com/rss.xml" aria-label="My blog's rss.xml">RSS</a>
		</nav>
	</header>
	<main>
		
<article>
	<h1>Let It Crash</h1>
	<time datetime=2024-07-15>July 15, 2024</time>
	<p><em>"Have you tried turning it off and on again?"</em></p>
<p>The question itself has become a meme. Yet, it is still worth asking that question and often the solution to a given problem. But why does turning it off and on again even work?</p>
<h2 id="state">State</h2>
<p>Most, if not all, software has some form of state. Be it a player's current coordinates in a video game, a focused element in a web browser, or user input. It's all stored in memory rather than a database. As software developers, we have some form of an idea of what the state of a program can look like. According to that idea, we write code and tests for it.</p>
<p>But developers are only humans and can't predict everything happening in a program. As a result, programs can end up in unpredictable and undesired states. Unexpected errors or function calls with wrong arguments can lead to this situation.</p>
<p>Let's look at the following function that takes in an int and returns a boolean.</p>
<pre data-lang="c" style="background-color:#151515;color:#e8e8d3;" class="language-c "><code class="language-c" data-lang="c"><span style="color:#8fbfdc;">bool </span><span style="color:#fad07a;">is_minor</span><span>(</span><span style="color:#8fbfdc;">int </span><span style="color:#ffb964;">age</span><span>) {
</span><span> </span><span style="color:#8fbfdc;">return</span><span> age &lt; </span><span style="color:#cf6a4c;">18</span><span>;
</span><span>}
</span></code></pre>
<p>The function checks if the passed age is less than 18. Nothing complicated. But what if the function receives a negative number? It's not wrong to ignore it, sure. But it goes against the name and purpose of the function. We should ask ourselves why a function that expects an age received a negative number. Throwing an error doesn't solve the problem. It's a bug. It requires fixing instead of coding around it. If we know that the parameter shouldn't be negative, we can assert it.</p>
<h2 id="assert">Assert</h2>
<p>Asserts take a boolean as an argument and check if the argument equals true. If it's not, it terminates the program and prints information to the console. That information usually includes the place where the assertion failed and its condition.</p>
<p>NASA's <a rel="nofollow noreferrer" href="https://web.eecs.umich.edu/~imarkov/10rules.pdf">The Power of 10: Rules for Developing Safety-Critical Code</a> states that there should be at least two asserts per function. <a rel="nofollow noreferrer" href="https://tigerbeetle.com/">Tigerbeetle</a>'s <a rel="nofollow noreferrer" href="https://github.com/tigerbeetle/tigerbeetle/blob/main/docs/TIGER_STYLE.md">Tiger Style</a> mentions it as well. Additionally, Tiger Style says that "the only correct way to handle corrupt code is to crash."</p>
<p>Many programming languages offer a built-in assert function. Some programming languages allow more arguments. As an example, a string to print on a failed condition. If there isn't a function like that, it is usually possible to create one yourself.</p>
<p>Taking our C function from before, we can add an assert to it.</p>
<pre data-lang="c" style="background-color:#151515;color:#e8e8d3;" class="language-c "><code class="language-c" data-lang="c"><span style="color:#8fbfdc;">bool </span><span style="color:#fad07a;">is_minor</span><span>(</span><span style="color:#8fbfdc;">int </span><span style="color:#ffb964;">age</span><span>) {
</span><span> assert(age &gt;= </span><span style="color:#cf6a4c;">0</span><span>);
</span><span> </span><span style="color:#8fbfdc;">return</span><span> age &lt; </span><span style="color:#cf6a4c;">18</span><span>;
</span><span>}
</span></code></pre>
<p>If the function receives a negative number, the program crashes, and we can find and fix the problem.</p>
<h2 id="to-crash-or-not-to-crash">To Crash or Not To Crash</h2>
<p>Knowing when to return (or throw) and handle errors and when to crash is difficult. Unfortunately, there is no guide on when to choose one over the other. You can't guarantee that everything works as expected. Such is the nature of software. But with asserts, you can improve the quality of your software. And you should.</p>

</article>

	</main>
</body>

</html>
